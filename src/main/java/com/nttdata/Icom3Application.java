package com.nttdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Icom3Application {

	public static void main(String[] args) {
		SpringApplication.run(Icom3Application.class, args);
	}

}
