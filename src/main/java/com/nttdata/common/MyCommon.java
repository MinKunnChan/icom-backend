package com.nttdata.common;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.common
 */

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;

import java.util.Map;
import java.util.Optional;

import static org.springframework.data.domain.Sort.Direction.DESC;
import static org.springframework.data.domain.Sort.Direction.fromString;

public class MyCommon {

    public static PageRequest getPageable(int page, int size, String direction, String orderBy) {
        Direction sortDirection = direction == null ? DESC : fromString(direction);
        if(orderBy == null){
            return PageRequest.of(page, size);
        }
        return PageRequest.of(page, size, sortDirection, orderBy);
    }

    public static PageRequest getPageable(int page, int size, Optional<String> direction, Optional<String> orderBy) {
        Direction sortDirection = direction.isPresent() ? fromString(direction.get()): DESC;
        if(orderBy.isPresent()){
            return PageRequest.of(page, size, sortDirection, orderBy.get());
        }
        return PageRequest.of(page, size);
    }

    public static PageRequest getPageable(Map<String, String> allQueries) {
        String page = allQueries.get("page");
        String size = allQueries.get("size");
        String direction = allQueries.get("direction");
        String orderBy = allQueries.get("orderBy");

        int pageNo;
        if(page == null || "".equals(page)){
            pageNo = 1;
        }else {
            pageNo = Integer.parseInt(page);
        }

        int sizeNo;
        if(size == null || "".equals(size)){
            sizeNo = 10;
        }else {
            sizeNo = Integer.parseInt(size);
        }

        if(orderBy == null || "".equals(orderBy)){
            return PageRequest.of(pageNo, sizeNo);
        }

        Direction sortDirection;
        if(direction == null || "".equals(direction)){
            sortDirection = DESC;
        }else {
            sortDirection = fromString(direction);
        }
        return PageRequest.of(pageNo, sizeNo, sortDirection, orderBy);

    }
}
