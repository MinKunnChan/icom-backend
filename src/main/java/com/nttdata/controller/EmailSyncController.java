package com.nttdata.controller;
/*
 * Created by kunnchan on 10/08/2020
 * package :  com.nttdata.controller
 */

import com.nttdata.model.EmailSyncEntity;
import com.nttdata.service.EmailSyncService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/email")
public class EmailSyncController {

    private final EmailSyncService emailSyncService;

    public EmailSyncController(EmailSyncService emailSyncService) {
        this.emailSyncService = emailSyncService;
    }

    @GetMapping("/first")
    public EmailSyncEntity getFirstEmailSync(){
        return emailSyncService.getFirstEmailSync();
    }

}
