package com.nttdata.controller;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.controller
 */

import com.nttdata.common.MyCommon;
import com.nttdata.respond.KeywordDetailListRespond;
import com.nttdata.respond.KeywordDetailRespond;
import com.nttdata.service.KeywordDetailService;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/keyword")
public class KeywordDetailController {

    public final KeywordDetailService keywordDetailService;

    public KeywordDetailController(KeywordDetailService keywordDetailService) {
        this.keywordDetailService = keywordDetailService;
    }

    @GetMapping("/{keywordId}")
    public KeywordDetailRespond getMessage(@PathVariable Integer keywordId){
        return keywordDetailService.getKeywordById(keywordId);
    }

    @GetMapping("/filter")
    public KeywordDetailListRespond getMessage(@RequestParam Map<String,String> allQueries){
        PageRequest pageable = MyCommon.getPageable(allQueries);
        return keywordDetailService.getKeywords(pageable, allQueries);
    }
}
