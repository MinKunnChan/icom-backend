package com.nttdata.controller;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.controller
 */

import com.nttdata.common.MyCommon;
import com.nttdata.respond.MessageListRespond;
import com.nttdata.respond.MessageRespond;
import com.nttdata.service.MessageService;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/messages")
public class MessageController {

    public final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/{keywordId}")
    public MessageRespond getMessage(@PathVariable String keywordId){
        return messageService.getMessageById(keywordId);
    }

    @GetMapping("/filter")
    public MessageListRespond getMessage(@RequestParam Map<String,String> allQueries){
        PageRequest pageable = MyCommon.getPageable(allQueries);
        return messageService.getMessages(pageable, allQueries);
    }

}
