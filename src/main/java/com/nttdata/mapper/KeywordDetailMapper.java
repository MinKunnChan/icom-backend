package com.nttdata.mapper;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.mapper
 */

import com.nttdata.model.KeywordDetail;
import com.nttdata.request.KeywordDetailRequest;
import com.nttdata.respond.KeywordDetailRespond;
import org.mapstruct.Mapper;

@Mapper
public interface KeywordDetailMapper {

    KeywordDetailRespond keywordDetailToKeywordDetailRespond(KeywordDetail keywordDetail);
    KeywordDetail keywordDetailRequestToKeywordDetail(KeywordDetailRequest keywordDetailRequest);
}
