package com.nttdata.mapper;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.mapper
 */

import com.nttdata.model.Message;
import com.nttdata.request.MessageRequest;
import com.nttdata.respond.MessageRespond;
import org.mapstruct.Mapper;

@Mapper
public interface MessageMapper {

    MessageRespond messageToMessageRespond(Message message);
    Message messageRequestToMessage(MessageRequest messageRequest);
}
