package com.nttdata.model;
/*
 * Created by kunnchan on 10/08/2020
 * package :  com.nttdata.model
 */

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class EmailSyncEntity {

    @Id
    @Column(name = "email_sync_state", length = 1000)
    private String emailSyncState;
}
