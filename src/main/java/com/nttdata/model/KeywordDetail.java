package com.nttdata.model;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.model
 */

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class KeywordDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "keyword_id")
    private Integer keywordId;

    @Column(name = "keyword", length = 100)
    private String keyword;

    @Column(name = "billing_account", length = 100)
    private String billingAccount;

    @Column(name = "channel", length = 1)
    private String channel;

    @Column(name = "channel_sms", length = 1)
    private String channelSms;

    @Column(name = "channel_email_subject", length = 1)
    private String channelEmailSubject;

    @Column(name = "channel_email_body", length = 1)
    private String channelEmailBody;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "match_type", length = 45)
    private String matchType;

    @Column(name = "status")
    private Integer status;

    @Column(name = "match_pattern", length = 1000)
    private String matchPattern;

    @Column(name = "receiver_email", length = 256)
    private String receiverEmail;

    @Column(name = "action_forward_to_ip", length = 1)
    private String actionForwardToIp;

    @Column(name = "action_download_to_bip", length = 1)
    private String actionDownloadToBip;

    @Column(name = "action_auto_response", length = 1)
    private String actionAutoResponse;

    @Column(name = "auto_response_template_id", length = 100)
    private String autoResponseTemplateId;

    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "last_updated_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdatedTime;

    @Column(name = "remarks")
    private String remarks;

}
