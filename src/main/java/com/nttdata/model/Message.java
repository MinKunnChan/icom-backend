package com.nttdata.model;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.model
 */

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.UUID;

@Data
@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

//    @GeneratedValue(generator = "UUID")
//    @GenericGenerator(
//            name = "UUID",
//            strategy = "org.hibernate.id.UUIDGenerator"
//    )
    @Column(name = "keyword_id", updatable = false, nullable = false, unique = true, columnDefinition = "BINARY(16)")
    private UUID keywordId;

    @Column(name = "keyword", length = 10)
    private String keyword;

    @Email
    private String emailAddress;

    private String billingAccount;
    private String regularExpression;

    @Column(name = "channel", length = 8)
    private String channel;

    private Boolean channelSms;
    private Boolean channelEmailBody;
    private Boolean channelEmailSubject;
    private Boolean autoResponseRequired;
    private Integer autoResponseTemplateId;
    private Boolean forwardToMessageFlag;
    private Boolean downloadBipFlag;

}
