package com.nttdata.repository;
/*
 * Created by kunnchan on 10/08/2020
 * package :  com.nttdata.repository
 */


import com.nttdata.model.EmailSyncEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;

@Repository
public interface EmailSyncRepository extends JpaRepository<EmailSyncEntity, String> {

    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<EmailSyncEntity> findFirstByOrderByEmailSyncState();

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    EmailSyncEntity save(EmailSyncEntity emailSyncEntity);
}
