package com.nttdata.repository;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.repository
 */

import com.nttdata.model.KeywordDetail;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeywordDetailRepository extends PagingAndSortingRepository<KeywordDetail, Integer>,
        QuerydslPredicateExecutor<KeywordDetail> {

}
