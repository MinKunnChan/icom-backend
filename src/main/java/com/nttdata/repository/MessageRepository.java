package com.nttdata.repository;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.repository
 */

import com.nttdata.model.Message;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MessageRepository extends PagingAndSortingRepository<Message, Integer>,
        QuerydslPredicateExecutor<Message> {

    Optional<Message> findByKeywordId(UUID keywordId);
}
