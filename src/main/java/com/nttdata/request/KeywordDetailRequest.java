package com.nttdata.request;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.request
 */

import lombok.Data;

@Data
public class KeywordDetailRequest {

    private Integer keywordId;
    private String keyword;
    private String billingAccount;
    private String channel;
    private String channelSms;
    private String channelEmailSubject;
    private String channelEmailBody;
    private String priority;
    private String matchType;
    private Integer status;
    private String matchPattern;
    private String receiverEmail;
    private String actionForwardToIp;
    private String actionDownloadToBip;
    private String actionAutoResponse;
    private String autoResponseTemplateId;
    private String remarks;
}
