package com.nttdata.request;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.request
 */

import lombok.Data;

import javax.validation.constraints.Email;
import java.util.UUID;

@Data
public class MessageRequest {

    private UUID keywordId;
    private String keyword;

    @Email
    private String emailAddress;

    private String billingAccount;
    private String regularExpression;
    private String channel;
    private Boolean channelSms;
    private Boolean channelEmailBody;
    private Boolean channelEmailSubject;
    private Boolean autoResponseRequired;
    private Integer autoResponseTemplateId;
    private Boolean forwardToMessageFlag;
    private Boolean downloadBipFlag;
}
