package com.nttdata.respond;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.respond
 */

import lombok.Data;

import java.util.List;

@Data
public class KeywordDetailListRespond {

    List<KeywordDetailRespond> content;

    Integer totalPages;
    Long totalElements;
    Boolean last;
    Integer size;
    Integer number;
    Boolean first;
    Integer numberOfElements;
    Boolean empty;
}
