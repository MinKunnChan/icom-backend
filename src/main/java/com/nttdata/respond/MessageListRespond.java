package com.nttdata.respond;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.respond
 */

import lombok.Data;

import java.util.List;

@Data
public class MessageListRespond {

    List<MessageRespond> content;
    Integer totalPages;
    Long totalElements;
    Boolean last;
    Integer size;
    Integer number;
    Boolean first;
    Integer numberOfElements;
    Boolean empty;
}
