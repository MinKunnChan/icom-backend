package com.nttdata.respond;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.respond
 */

import lombok.Data;

import java.util.UUID;

@Data
public class MessageRespond {

    private UUID keywordId;
    private String keyword;
    private String emailAddress;
    private String billingAccount;
    private String regularExpression;
    private String channel;
    private Boolean channelSms;
    private Boolean channelEmailBody;
    private Boolean channelEmailSubject;
    private Boolean autoResponseRequired;
    private Integer autoResponseTemplateId;
    private Boolean forwardToMessageFlag;
    private Boolean downloadBipFlag;

}
