package com.nttdata.service;
/*
 * Created by kunnchan on 10/08/2020
 * package :  com.nttdata.service
 */


import com.nttdata.model.EmailSyncEntity;

public interface EmailSyncService {
    EmailSyncEntity getFirstEmailSync();
}
