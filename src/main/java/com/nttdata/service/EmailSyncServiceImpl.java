package com.nttdata.service;
/*
 * Created by kunnchan on 10/08/2020
 * package :  com.nttdata.service
 */

import com.nttdata.model.EmailSyncEntity;
import com.nttdata.repository.EmailSyncRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class EmailSyncServiceImpl implements EmailSyncService{

    private final EmailSyncRepository emailSyncRepository;

    public EmailSyncServiceImpl(EmailSyncRepository emailSyncRepository) {
        this.emailSyncRepository = emailSyncRepository;
    }

    @Transactional
    @Override
    public EmailSyncEntity getFirstEmailSync() {
        Optional<EmailSyncEntity> stateOptional = emailSyncRepository.findFirstByOrderByEmailSyncState();
        if(stateOptional.isPresent()){
            EmailSyncEntity emailSyncEntity = stateOptional.get();
            emailSyncRepository.delete(emailSyncEntity);
        }
        String uuid = UUID.randomUUID().toString();
        EmailSyncEntity entity = new EmailSyncEntity();
        entity.setEmailSyncState(uuid);
        return  emailSyncRepository.save(entity);
    }
}
