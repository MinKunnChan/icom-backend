package com.nttdata.service;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.service
 */


import com.nttdata.respond.KeywordDetailListRespond;
import com.nttdata.respond.KeywordDetailRespond;
import org.springframework.data.domain.PageRequest;

import java.util.Map;

public interface KeywordDetailService {
    KeywordDetailRespond getKeywordById(Integer keywordId);

    KeywordDetailListRespond getKeywords(PageRequest pageable, Map<String, String> allQueries);
}
