package com.nttdata.service;
/*
 * Created by kunnchan on 15/08/2020
 * package :  com.nttdata.service
 */

import com.nttdata.exception.MyResourceNotFoundException;
import com.nttdata.mapper.KeywordDetailMapper;
import com.nttdata.model.KeywordDetail;
import com.nttdata.model.QKeywordDetail;
import com.nttdata.repository.KeywordDetailRepository;
import com.nttdata.respond.KeywordDetailListRespond;
import com.nttdata.respond.KeywordDetailRespond;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class KeywordDetailServiceImpl implements KeywordDetailService{

    private final KeywordDetailRepository keywordDetailRepository;
    private KeywordDetailMapper mapper;

    public KeywordDetailServiceImpl(KeywordDetailRepository keywordDetailRepository) {
        this.keywordDetailRepository = keywordDetailRepository;
        mapper = Mappers.getMapper(KeywordDetailMapper.class);
    }

    @Override
    public KeywordDetailRespond getKeywordById(Integer keywordId) {
        Optional<KeywordDetail> messageOptional = keywordDetailRepository.findById(keywordId);

        if(messageOptional.isPresent()){
            return mapper.keywordDetailToKeywordDetailRespond(messageOptional.get());
        }
        throw new MyResourceNotFoundException("Not found for id "+ keywordId);
    }

    @Override
    public KeywordDetailListRespond getKeywords(PageRequest pageable, Map<String, String> allQueries) {
        BooleanExpression query = getQuery(allQueries);
        Page<KeywordDetail> page = keywordDetailRepository.findAll(query, pageable);

        KeywordDetailListRespond dto = new KeywordDetailListRespond();
        dto.setTotalPages(page.getTotalPages());
        dto.setTotalElements(page.getTotalElements());
        dto.setLast(page.isLast());
        dto.setSize(page.getSize());
        dto.setNumber(page.getNumber());
        dto.setFirst(page.isFirst());
        dto.setNumberOfElements(page.getNumberOfElements());
        dto.setEmpty(page.isEmpty());

        List<KeywordDetailRespond> contents = new ArrayList<>();
        for (KeywordDetail keywordDetail: page.getContent()) {
            contents.add(mapper.keywordDetailToKeywordDetailRespond(keywordDetail));
        }
        dto.setContent(contents);

        return dto;
    }

    private BooleanExpression getQuery(Map<String, String> allQueries){
        BooleanExpression expression = Expressions.asBoolean(true).isTrue();

        QKeywordDetail query = QKeywordDetail.keywordDetail;

        //    private String ;
        //    private String ;
        //    private String ;
        //    private String ;
        //    private String ;
        //    private String ;
        //    private String ;
        //

        String keywordId = allQueries.get("keywordId");
        String keyword = allQueries.get("keyword");
        String billingAccount = allQueries.get("billingAccount");
        String channel = allQueries.get("channel");
        String channelSms = allQueries.get("channelSms");
        String channelEmailBody = allQueries.get("channelEmailBody");
        String channelEmailSubject = allQueries.get("channelEmailSubject");
        String priority = allQueries.get("priority");
        String matchType = allQueries.get("matchType");
        String status = allQueries.get("status");
        String matchPattern = allQueries.get("matchPattern");
        String receiverEmail = allQueries.get("receiverEmail");
        String actionForwardToIp = allQueries.get("actionForwardToIp");
        String actionDownloadToBip = allQueries.get("actionDownloadToBip");
        String actionAutoResponse = allQueries.get("actionAutoResponse");
        String autoResponseTemplateId = allQueries.get("autoResponseTemplateId");
        String remarks = allQueries.get("remarks");

        if(isNumeric(keywordId)){
            expression = expression.and(query.keywordId.eq(Integer.parseInt(keywordId)));
        }
        if(isNotNullString(keyword)){
            expression = expression.and(query.keyword.equalsIgnoreCase(keyword));
        }
        if(isNotNullString(billingAccount)){
            expression = expression.and(query.billingAccount.equalsIgnoreCase(billingAccount));
        }
        if(isNotNullString(channel)){
            expression = expression.and(query.channel.equalsIgnoreCase(channel));
        }
        if(isNotNullString(channelSms)){
            expression = expression.and(query.channelSms.equalsIgnoreCase(channelSms));
        }
        if(isNotNullString(channelEmailBody)){
            expression = expression.and(query.channelEmailBody.equalsIgnoreCase(channelEmailBody));
        }
        if(isNotNullString(channelEmailSubject)){
            expression = expression.and(query.channelEmailSubject.equalsIgnoreCase(channelEmailSubject));
        }
        if(isNotNullString(priority)){
            expression = expression.and(query.priority.eq(Integer.parseInt(priority)));
        }
        if(isNotNullString(matchType)){
            expression = expression.and(query.matchType.eq(matchType));
        }
        if(isNumeric(status)){
            expression = expression.and(query.status.eq(Integer.parseInt(status)));
        }
        if(isNotNullString(matchPattern)){
            expression = expression.and(query.matchPattern.eq(matchPattern));
        }
        if(isNotNullString(receiverEmail)){
            expression = expression.and(query.receiverEmail.equalsIgnoreCase(receiverEmail));
        }
        if(isNotNullString(actionForwardToIp)){
            expression = expression.and(query.actionForwardToIp.equalsIgnoreCase(actionForwardToIp));
        }
        if(isNumeric(actionDownloadToBip)){
            expression = expression.and(query.actionDownloadToBip.equalsIgnoreCase(actionDownloadToBip));
        }
        if(isNotNullString(actionAutoResponse)){
            expression = expression.and(query.actionAutoResponse.equalsIgnoreCase(actionAutoResponse));
        }
        if(isNumeric(autoResponseTemplateId)){
            expression = expression.and(query.autoResponseTemplateId.equalsIgnoreCase(autoResponseTemplateId));
        }
        if(isNotNullString(remarks)){
            expression = expression.and(query.remarks.equalsIgnoreCase(remarks));
        }
        return expression;
    }

    boolean isNotNullString(String string){
        return string != null && !"".equals(string);
    }

    boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
