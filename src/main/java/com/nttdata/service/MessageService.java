package com.nttdata.service;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.service
 */


import com.nttdata.request.MessageRequest;
import com.nttdata.respond.MessageRespond;
import com.nttdata.respond.MessageListRespond;
import org.springframework.data.domain.PageRequest;

import java.util.Map;

public interface MessageService {
    MessageRespond getMessageById(String keywordId);
    MessageRespond saveMessage(MessageRequest messageRequest);
    void deleteMessage(Integer id);

    MessageListRespond getMessages(PageRequest pageable, Map<String, String> allQueries);
}
