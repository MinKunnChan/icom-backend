package com.nttdata.service;
/*
 * Created by kunnchan on 04/08/2020
 * package :  com.nttdata.service
 */

import com.nttdata.exception.MyResourceNotFoundException;
import com.nttdata.mapper.MessageMapper;
import com.nttdata.model.Message;
import com.nttdata.model.QMessage;
import com.nttdata.repository.MessageRepository;
import com.nttdata.request.MessageRequest;
import com.nttdata.respond.MessageListRespond;
import com.nttdata.respond.MessageRespond;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Slf4j
@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;
    private MessageMapper mapper;

    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
        mapper = Mappers.getMapper(MessageMapper.class);
    }

    @Override
    public MessageRespond getMessageById(String keywordId) {
        Optional<Message> messageOptional = messageRepository.findByKeywordId(UUID.fromString(keywordId));

        if(messageOptional.isPresent()){
            return mapper.messageToMessageRespond(messageOptional.get());
        }
        throw new MyResourceNotFoundException("Not found for id "+ keywordId);
    }

    @Transactional
    @Override
    public MessageRespond saveMessage(MessageRequest messageRequest) {
        Message message = mapper.messageRequestToMessage(messageRequest);
        return mapper.messageToMessageRespond(messageRepository.save(message));
    }

    @Override
    public void deleteMessage(Integer id) {
        messageRepository.deleteById(id);
    }

    @Override
    public MessageListRespond getMessages(PageRequest pageable, Map<String, String> allQueries) {

        BooleanExpression query = getQuery(allQueries);
        Page<Message> page = messageRepository.findAll(query, pageable);

        MessageListRespond dto = new MessageListRespond();
        dto.setTotalPages(page.getTotalPages());
        dto.setTotalElements(page.getTotalElements());
        dto.setLast(page.isLast());
        dto.setSize(page.getSize());
        dto.setNumber(page.getNumber());
        dto.setFirst(page.isFirst());
        dto.setNumberOfElements(page.getNumberOfElements());
        dto.setEmpty(page.isEmpty());

        List<MessageRespond> contents = new ArrayList<>();
        for (Message message: page.getContent()) {
            contents.add(mapper.messageToMessageRespond(message));
        }
        dto.setContent(contents);

        return dto;
    }

    private BooleanExpression getQuery(Map<String, String> allQueries){
        BooleanExpression expression = Expressions.asBoolean(true).isTrue();

        QMessage qMessage = QMessage.message;

        String keywordId = allQueries.get("keywordId");
        String keyword = allQueries.get("keyword");
        String emailAddress = allQueries.get("emailAddress");
        String billingAccount = allQueries.get("billingAccount");
        String regularExpression = allQueries.get("regularExpression");
        String channel = allQueries.get("channel");
        String channelSms = allQueries.get("channelSms");
        String channelEmailBody = allQueries.get("channelEmailBody");
        String channelEmailSubject = allQueries.get("channelEmailSubject");
        String autoResponseRequired = allQueries.get("autoResponseRequired");
        String autoResponseTemplateId = allQueries.get("autoResponseTemplateId");
        String forwardToMessageFlag = allQueries.get("forwardToMessageFlag");
        String downloadBipFlag = allQueries.get("downloadBipFlag");

        if(isNoNull(keywordId)){
            expression = expression.and(qMessage.keywordId.eq(UUID.fromString(keywordId)));
        }
        if(isNoNull(keyword)){
            expression = expression.and(qMessage.keyword.equalsIgnoreCase(keyword));
        }
        if(isNoNull(emailAddress)){
            expression = expression.and(qMessage.emailAddress.equalsIgnoreCase(emailAddress));
        }
        if(isNoNull(billingAccount)){
            expression = expression.and(qMessage.billingAccount.equalsIgnoreCase(billingAccount));
        }
        if(isNoNull(regularExpression)){
            expression = expression.and(qMessage.regularExpression.equalsIgnoreCase(regularExpression));
        }
        if(isNoNull(channel)){
            expression = expression.and(qMessage.channel.equalsIgnoreCase(channel));
        }
        if(isNoNull(channelSms)){
            expression = expression.and(qMessage.channelSms.eq(Boolean.parseBoolean(channelSms)));
        }
        if(isNoNull(channelEmailBody)){
            expression = expression.and(qMessage.channelEmailBody.eq(Boolean.parseBoolean(channelEmailBody)));
        }
        if(isNoNull(channelEmailSubject)){
            expression = expression.and(qMessage.channelEmailSubject.eq(Boolean.parseBoolean(channelEmailSubject)));
        }
        if(isNoNull(autoResponseRequired)){
            expression = expression.and(qMessage.autoResponseRequired.eq(Boolean.parseBoolean(autoResponseRequired)));
        }
        if(isNoNull(autoResponseTemplateId)){
            expression = expression.and(qMessage.autoResponseTemplateId.eq(Integer.parseInt(autoResponseTemplateId)));
        }
        if(isNoNull(forwardToMessageFlag)){
            expression = expression.and(qMessage.forwardToMessageFlag.eq(Boolean.parseBoolean(forwardToMessageFlag)));
        }
        if(isNoNull(downloadBipFlag)){
            expression = expression.and(qMessage.downloadBipFlag.eq(Boolean.parseBoolean(downloadBipFlag)));
        }
        return expression;
    }

    boolean isNoNull(String string){
        return string != null && !"".equals(string);
    }
}
