create table icomm3.email_sync (`email_sync_state` varchar(1000) not null,
primary key (`email_sync_state`)) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE TABLE icomm3.keyword_detail (
	keyword_id INT(11) NOT NULL,
	keyword	varchar(100),
	billing_account	varchar(100),
	`channel`	varchar(1),
	channel_sms	varchar(1),
	channel_email_subject	varchar(1),
	channel_email_body	varchar(1),
	priority	int(11),
	match_type	varchar(45),
	status	int(11),
	match_pattern	varchar(1000),
	receiver_email	varchar(256),
	action_forward_to_ip	varchar(1),
	action_download_to_bip	varchar(1),
	action_auto_response	varchar(1),
	auto_response_template_id	varchar(100),
	created_time	timestamp,
	last_updated_time	timestamp,
	remarks	varchar(100),
  PRIMARY KEY (keyword_id)
  );