package com.nttdata.repository;

import com.nttdata.model.EmailSyncEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Transactional
@Rollback
@SpringBootTest
class EmailSyncRepositoryTest {

    @Autowired
    private EmailSyncRepository emailSyncRepository;

    EmailSyncEntity emailSyncEntity;

    @BeforeEach
    void setUp() {
        EmailSyncEntity entity = new EmailSyncEntity();
        entity.setEmailSyncState("test");

        emailSyncEntity = emailSyncRepository.save(entity);
    }

    @Test
    void findFirstByOrderByEmailSyncState() {

        EmailSyncEntity state = emailSyncRepository.findFirstByOrderByEmailSyncState()
                .orElse(null);
        assertNotNull(state);
        assertEquals(state.getEmailSyncState(), emailSyncEntity.getEmailSyncState());

    }

    @Test
    void save() {

        assertNotNull(emailSyncEntity);
        assertEquals("test", emailSyncEntity.getEmailSyncState());
    }
}